require 'net/http'
require 'nokogiri'
require 'json'

module WebParser
    class Source
        def initialize(address)
            if address.instance_of? String
                @address = address
            else
                raise TypeError, 'address must be a String'
            end
        end

        def get_rates
            retrieve_data
        end

        private
        def retrieve_data
            {}
        end
    end

    class OpenExchange < Source
        private
        def retrieve_data
            response = Net::HTTP.get_response(URI.parse(@address))
            data = JSON.parse(response.body)
            if data["rates"]
                usd = (1 / data["rates"]["EUR"]).round(4)
                aud = (usd * data["rates"]["AUD"]).round(4)
                nok = (usd * data["rates"]["NOK"]).round(4)
                { :AUD => aud, :USD => usd, :NOK => nok }
            else
                {}
            end
        end
    end

    class CurrencyApi < Source
        private
        def retrieve_data
            usd = get_rate("USD")
            aud = get_rate("AUD")
            nok = get_rate("NOK")
            { :AUD => aud, :USD => usd, :NOK => nok }
        end

        def get_rate(currency)
            address = @address % currency
            response = Net::HTTP.get_response(URI.parse(address))
            data = JSON.parse(response.body)
            if data["success"]
                data["rate"]
            else
                data["message"]
            end
        end
    end

    class ECB < Source
        private
        def retrieve_data
            response = Net::HTTP.get_response(URI.parse(@address))
            data = Nokogiri::XML(response.body).remove_namespaces!
            aud = data.xpath('//Cube[@currency="AUD"]/@rate')[0].value
            usd = data.xpath('//Cube[@currency="USD"]/@rate')[0].value
            nok = data.xpath('//Cube[@currency="NOK"]/@rate')[0].value
            { :AUD => aud, :USD => usd, :NOK => nok }
        end
    end
end
