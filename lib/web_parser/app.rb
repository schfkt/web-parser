require 'sinatra/base'

require_relative 'source'
require_relative 'config'

module WebParser
    class App < Sinatra::Base
        get '/' do
            # openexchange
            address = WebParser::CONFIG["addresses"]["openexchangerates"]
            address += WebParser::CONFIG["keys"]["openexchangerates"]
            @openexchange = WebParser::OpenExchange.new(address).get_rates rescue {}

            # currency-api
            address = WebParser::CONFIG["addresses"]["currency_api"]
            address += WebParser::CONFIG["keys"]["currency_api"]
            @currency_api = WebParser::CurrencyApi.new(address).get_rates rescue {}

            # ecb
            address = WebParser::CONFIG["addresses"]["ecb"]
            @ecb = WebParser::ECB.new(address).get_rates rescue {}

            # render template
            haml :index
        end

        run!
    end
end